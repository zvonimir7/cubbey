﻿using UnityEngine;

public class GameOverlay : MonoBehaviour {
    public void QuitGame() {
        Application.Quit();
    }
}
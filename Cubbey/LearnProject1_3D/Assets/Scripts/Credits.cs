﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {

    public void Quit() {
        Debug.Log("Game has been closed!");
        Application.Quit();
    }

    public void GoToMenu() {
        SceneManager.LoadScene(0);
    }

    public void RestartGame() {
        SceneManager.LoadScene(1);
    }
}